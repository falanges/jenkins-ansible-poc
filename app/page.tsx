import Image from "next/image";
import fs from "fs";

export default function Home() {
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      <h1>
        This is a POC to demonstrate an Ansible common configuration with
        Jenkins and other cool stuff{" "}
      </h1>
    </main>
  );
}
